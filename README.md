# OpenML dataset: New-Delhi-Rental-Listings

https://www.openml.org/d/43837

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The dataset is from a rental price prediction project I did. Includes different types of properties (Apartments, Independent floors, Independent houses, Villas etc.)
It contains 12000 rental listings from a popular real estate website. It can be used for rental prediction projects, analysis of areas of affluence etc.
Content
The dataset multiple quantitative, categorical and co-ordinate features including :

Data about the houses : 
sizesqft, 
propertyType,
bedrooms,
Data about the locality of the house :
latitude,
longitude,
localityName,
suburbName,
cityName,
Asking Rent :
price,
Property agency :
companyName,
Distance to closest landmarks (geodesic distance, not driving-road distance)  :
closestmterostationkm,
APdistkm (Indira Gandhi International Airport),
Aiimsdistkm (All India Institute of Medical Science - major government hospital),
NDRLWdist_km (New Delhi Railway Station), 

Heatmap of Data
Red Vmax for monthly rent of Rs. 2lakh/mo and above.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43837) of an [OpenML dataset](https://www.openml.org/d/43837). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43837/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43837/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43837/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

